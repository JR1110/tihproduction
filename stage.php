<title>Stage</title>
<body id="stage">
    <?php
        include 'heading.php';     //includes the heading
        include 'server_connection.php';	//includes the server connection file

        header("Refresh:9");       //refreshing the page

        $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
        }

        $database = $dbname;

        $checkFile = fopen('nightActions/currentNo.txt', 'r') or die ('Unable to open');

        $itemNo = fread($checkFile, filesize('nightActions/currentNo.txt'));

        $checkAgainst = fopen('stageCurrent.txt', 'w+') or die ('Unable to open');

        $checkNo = fread($checkAgainst, filesize('stageCurrent.txt'));

        if ($checkNo != $itemNo)
        {
            $allRead = explode('|', $itemNo);
            $item = $allRead[0];
            $night = $allRead[1];

            $nightSQL = 'SELECT i.* FROM item i JOIN eventDetails e ON e.itemID=i.itemID WHERE e.orderOnNight = '.$item.' AND eventDate = "'.$night.'"';

            $result = $conn->query($nightSQL);

            while ($row = $result->fetch_assoc())
            {
                echo '<div id="firstThree" class="headlines">';

                    echo '<h1>'.$row['title'].'</h1>';
                    echo '<table class="table table-condensed">';
                        echo '<tr>';
                            echo '<th class="bigTop">Intro : '.$row['intro'].'</th>';
                            echo '<th class="bigTop">Structure : '.$row['structure'].'</th>';
                            echo '<td class="bigTop '.($row['ppt'] == 1 ? "success" : "").'">PPT : '.($row['ppt'] == 1 ? "Yes" : "No").'</td>';
                        echo '</tr>';
                        echo '<tr>';
                            echo '<td class="bigTop" colspan="3">Info : '.$row['info'].'</td>';
                        echo '</tr>';
                    echo '</table>';

                echo '</div>';

                echo '<div class="col-md-6" id="leftColumn">';

                $nextFiveSQL = 'SELECT i.title FROM item i JOIN eventDetails e ON e.itemID = i.itemID WHERE orderOnNight > '.$item.' AND eventDate = "'.$night.'" ORDER BY orderOnNight LIMIT 2';

                $nFResult = $conn->query($nextFiveSQL);

                while ($nFRow = $nFResult->fetch_assoc())
                {
                    echo '<h3> Next : '.$nFRow['title'].'</h3>';
                }

                echo '<br><h2> Notes : </h2>';

                $notesFile = fopen('nightActions/notes.txt', 'r');

                $notes = fread($notesFile, filesize('nightActions/notes.txt'));

                fclose($notesFile);

                echo '<textarea class="form-control" rows="3" id="notes">'.$notes.'</textarea>';

                echo '</div>';

                echo '<div class="col-md-6"  id="rightColumn">';

                echo '<table class="table">';

                $fullBand = array();

                $fullBandSQL = 'SELECT b.instrumentID FROM bandDesc b WHERE b.bandID = (SELECT e.houseBandID FROM event e WHERE e.eventDate = "'.$night.'")';

                $fullBandResult = $conn->query($fullBandSQL);

                while($fullBandRow = $fullBandResult->fetch_assoc())
                {
                    $fullBand[] = $fullBandRow['instrumentID'];
                }

                for ($i = 0; $i < sizeof($fullBand); $i++)
                {
                    echo '<tr>';

                    $getInstrumentSQL = 'SELECT * FROM instrument WHERE instrumentID = '.$fullBand[$i];

                    $insResult = $conn->query($getInstrumentSQL);

                    while ($insRow = $insResult->fetch_assoc())
                    {
                        echo '<th style="width:70%">' . $insRow['instrumentDesc'] .($insRow['person'] == null ? '' : ', '.$insRow['person']). '</th>';
                    }

                    $bandSQL = 'SELECT * FROM songBand WHERE itemID = ' . $row['itemID'].' AND instrumentID = '.$fullBand[$i];

                    $bandResult = $conn->query($bandSQL);

                    if(mysqli_num_rows($bandResult) == null)
                    {
                        echo '<td> No </td>';
                    } else {
                        echo '<td class="stageSuccess"><b> Yes </b></td>';
                    }

                    echo'</tr>';
                }

                echo '</table>';

                echo '</div>';
            }

            fwrite($checkAgainst, $itemNo);

        }

        fclose($checkFile);

        fclose($checkAgainst);
    ?>
</body>
