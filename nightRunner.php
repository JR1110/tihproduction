<title>Run Night</title>
<body onload="scrollPlan()">
    <?php
    include 'heading.php';     //includes the heading
    include 'server_connection.php';	//includes the server connection file

    header("Refresh:30");       //refreshing the page

    $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
    }

    $database = $dbname;

    $nightPlan = array();
    $currentItemNo = 0;

    if ($_SERVER['REQUEST_METHOD'] == 'GET' && $nightPlan == null)
    {
        currentCheck($conn);

        $i = 0;

        //getting the details of the event selected in order of the night plan
        $eventSQL = 'SELECT i.*, e.expectedStart, e.orderOnNight, e.status FROM eventDetails e JOIN item i ON e.itemID = i.itemID WHERE eventDate = "'.$_GET['night'].'" ORDER BY orderOnNight ASC';
        $eventDet = $conn->query($eventSQL);

        while ($row = $eventDet->fetch_assoc())
        {

            $nightPlan[$i] = array("itemName"=>$row['title'],
                "itemID"=>$row['itemID'],
                "structure"=>$row['structure'],
                "intro"=>$row['intro'],
                "type"=>$row['type'],
                "info"=>$row['info'],
                "duration"=>$row['duration'],
                "startTime"=>$row['expectedStart'],
                "ppt"=>$row['ppt'],
                "status"=>$row['status']);

            $i++;

        }

    }

    ?>

    <div id="bandSearch">
        <?php
        if (!isset($_GET['night'])) {

            echo '<h1>Night Runner</h1><br>';

            echo '<label>Select Event : </label>';

            echo '<form action="" method="get">';
            $select = '<select name="night">';

            $sql = 'SELECT * FROM event';                //selecting all bands in the system
            $result = $conn->query($sql);

            while ($row = $result->fetch_assoc()) {
                $select .= '<option value="' . $row['eventDate'] . '">' . $row['eventDate'] . '</option>';    //add each band to the dropdown list
            }

            $select .= '</select>';

            echo $select;       //displays the dropdown list created

            echo '<input type="submit">';
            echo '</form>';
        }
        ?>
    </div>

    <?php
    if (isset($_GET['night'])) {
		
		echo '<div id="allLeft" class="col-md-8 col-sm-8">';
        echo '<div id="firstThree" class="container">';

        for ($j = 0; sizeof($nightPlan); $j++)
        {
            if ($nightPlan[$j]["status"] == "current")
            {
                $currentItemNo = $j;
                break;
            }
        }

        echo '<h2>Current Item : '.$nightPlan[$currentItemNo]["itemName"].'</h2>';
        echo '<h3>Next Item : '.$nightPlan[$currentItemNo+1]["itemName"].'</h3>';
        echo '<h3>Next Item : '.$nightPlan[$currentItemNo+2]["itemName"].'</h3>';

        echo '</div>';

        echo '<div id="leftColumn" class="col-md-6 col-sm-6">';

            echo '<h3>Current item duration : '.$nightPlan[$currentItemNo]["duration"].'</h3>';

            //insert time calculation for error
            $errorMinusDuration = 0;

            $currentStart = strtotime($nightPlan[$currentItemNo]['startTime']);
            $now = time();

            $errorMinusDuration;

            $errorMinusDuration = $now - $currentStart;         //calculating the difference in the times

            $errorHours = ($errorMinusDuration/3600 < 0 ? ceil($errorMinusDuration/3600) : floor($errorMinusDuration / 3600));                                        //the hours difference is the floor of total diff / 60sec*60min (3600)

            $errorMinutes = (($errorMinusDuration - ($errorHours*3600)) < 0 ? ceil(($errorMinusDuration - ($errorHours*3600)) / 60) : floor(($errorMinusDuration - ($errorHours*3600)) / 60));                 //the min time is thew floor of total time without hours / 60sec

            $errorSeconds = floor($errorMinusDuration - ($errorHours*3600) - ($errorMinutes*60));   //the second time is whats remaining after hours and mins taken away

            $minusErrorBool = false;        //boolean to store if the error is minus (ahead of schedule!)

            if ($errorHours < 0)        //if the hours are less than 0, negative
            {
                $errorHours = $errorHours * -1;         //times it by -1 for displaying it as a positive integer
                $minusErrorBool = true;                 //set the minus error boolean to be true
            }

            if ($errorMinutes < 0)        //if the hours are less than 0, negative
            {
                $errorMinutes = $errorMinutes * -1;         //times it by -1 for displaying it as a positive integer
                $minusErrorBool = true;                 //set the minus error boolean to be true
            }

            if ($errorSeconds < 0)        //if the hours are less than 0, negative
            {
                $errorSeconds = $errorSeconds * -1;         //times it by -1 for displaying it as a positive integer
                $minusErrorBool = true;                 //set the minus error boolean to be true
            }

            if ($minusErrorBool)
            {
                echo '<table class="table table-borderless"><tr><th>Error  : </th><th class="success"> - '.sprintf("%02d", $errorHours).':'.sprintf("%02d",$errorMinutes).':'.sprintf("%02d",$errorSeconds).'</th></tr></table>';
            } else {
                echo '<table class="table table-borderless"><tr><th>Error  : </th><td class="danger"> '.sprintf("%02d", $errorHours).':'.sprintf("%02d",$errorMinutes).':'.sprintf("%02d",$errorSeconds).'</td></tr></table>';
            }

            echo '<p>* Error is : current time - current items start time</p>';

            echo '<h2> Notes : </h2>';

            echo '<form action = "nightActions/notes.php?night='.$_GET['night'].'" method="POST">';

                $notesFile = fopen('nightActions/notes.txt', 'r');

                $notes = fread($notesFile, filesize('nightActions/notes.txt'));

                fclose($notesFile);

                echo '<textarea class="form-control" rows="5" name="notes">'.$notes.'</textarea>';

                echo '<input type="submit" class="btn btn-default btn-block" value="Send Note!">';

            echo '</form>';

        echo '</div>';

        echo '<div id="midColumn" class="col-md-6 col-sm-6">';

            echo '<h3>Beginning of night : </h3>';

            echo '<a href="nightActions\startNight.php?night='.$_GET['night'].'" class="btn btn-default btn-block btn-primary">Start the Night</a>';

            echo '<h3>Changing current item : </h3>';

            echo '<a href="nightActions\previousItem.php?night='.$_GET['night'].'&plan='.$currentItemNo.'" class="btn btn-default btn-block btn-danger">Previous Item</a>';

            echo '<a href="nightActions\nextItem.php?night='.$_GET['night'].'&plan='.$currentItemNo.'" class="btn btn-default btn-block btn-success">Next Item</a>';

            echo '<h3>Downtime : </h3>';

            echo '<form action="nightActions\addDowntime.php">';

                $downtimeSelect = '<select name="downtime" style="width: 100%">';

                $SQLDowntime = 'SELECT title, itemID FROM item WHERE type = "Downtime"';        //sql to get all downtime items
                $result = $conn->query($SQLDowntime);                                           //running query

                while ($row = $result->fetch_assoc())                                           //while there are results
                {
                    $downtimeSelect .= '<option value="'.$row['itemID'].'">'.$row['title'].'</option>';         //add them as an option
                }

                $downtimeSelect .= '</select>';         //finish the dropdown

                echo $downtimeSelect;

                echo '<input type="hidden" name="night" value='.$_GET['night'].'>';

                echo '<input type="submit" class="btn btn-default btn-block" value="Add Downtime">';

            echo '</form>';

        echo '</div>';
		
		echo '</div>';

        echo '<div id="rightColumn" class="col-md-4 col-sm-4">';

            echo '<h2>Night Plan ('.$_GET['night'].')</h2>';

            echo '<a href="preloadProducer.php?use=Edit&night='.$_GET['night'].'" target="_blank" class="btn btn-default btn-inline btn-success" style="width: 45%; margin: 5px;">Edit Plan</a>';
            echo '<a href="preloadProducer.php?use=Load&night='.$_GET['night'].'" class="btn btn-default btn-inline btn-warning" style="width: 45%; margin: 5px;">Reload Plan</a>';

            echo '<table class="table table-hover table-condensed"><tbody style="display: block; max-height: 300px !important; overflow-y: auto !important;">';

            for ($i = 0; $i < sizeof($nightPlan); $i++)
            {
                $check = ($nightPlan[$i]["status"] == "current" ? "class='success' id='current' " : "");
                echo '<tr '.$check.'>';

                echo '<th style="width: 300px;"><a href="songEdit.php?night='.$_GET['night'].'&item='.$nightPlan[$i]["itemID"].'">'.$nightPlan[$i]["itemName"].'</a></th>';
                echo '<td style="width: 150px;">'.$nightPlan[$i]["startTime"].'</td>';     //display the time the item is due to start

                echo '</tr>';
            }

            echo '</tbody></table>';

            echo '<button class="btn btn-default btn-block" onclick="scrollPlan()">To Current Item</button><br>';

        echo '</div>';

    }

    ?>

    <script>

        function scrollPlan()
        {
            var currentTo = document.getElementById("current");
            currentTo.scrollIntoView();
        }

    </script>

    <?php
    function currentCheck($conn)         //checks if a current item exists
    {
        $check = 0;

        $SQL = "SELECT status FROM eventDetails WHERE eventDate='".$_GET['night']."' AND status = 'current' ";

        $result = $conn->query($SQL);

        while ($row = $result->fetch_assoc())
        {
            $check++;
        }

        if ($check == 0)
        {
            $checkFile = fopen('nightActions/currentNo.txt', 'r') or die ('Unable to open');

            $itemNo = fread($checkFile, filesize('nightActions/currentNo.txt'));

            $allRead = explode('|', $itemNo);
            $item = $allRead[0];
            $night = $allRead[1];

            if ($item != null && $night != null) {

                $currentCreator = 'UPDATE eventDetails SET status="current" WHERE eventDate="' . $night . '" AND orderOnNight=' . $item;

                $conn->query($currentCreator);
            } else {
                $currentCreator = 'UPDATE eventDetails SET status="current" WHERE eventDate="'.$_GET['night'].'" AND orderOnNight = 1';

                $conn->query($currentCreator);
            }
        }
    }
    ?>
</body>