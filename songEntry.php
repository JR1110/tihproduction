<title>Enter Songs</title>
<body>
    <?php
        include 'heading.php';     //includes the heading
        include 'server_connection.php';	//includes the server connection file

        $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
        }

        $database = $dbname;
    ?>

    <div id="bandSearch">

        <label>Select Band : </label>

        <form action='' method='get'>
                <?php                                           //form to select what band is performing the item
                    $select='<select name="band">';

                    $sql = 'SELECT * FROM band';                //selecting all bands in the system
                    $result = $conn->query($sql);

                    while ($row = $result->fetch_assoc())
                    {
                        $select.='<option value="'.$row['bandID'].'">'.$row['bandDesc'].'</option>';    //add each band to the dropdown list
                    }

                    $select.='</select>';

                    echo $select;
                ?>
            <input type='submit'>
        </form>

    </div>

    <div id="leftColumn" class="col-md-8">
        <?php
            if (isset($_GET['band']))                           //if a band has been chosen
            {
                echo '<h2>Enter New Item</h2>';
                $availableInstruments = array();                //array that will store all the available instruments for that band

                $bandNo = $_GET['band'];                        //getting what band is performing

                //Creating the basic form text boxes to take in the variables
                $songForm = '<form action ="" method="post">';
                $songForm .= '<div class="form-group"><label>Title : </label><input type="text" name="title" maxlength="100" placeholder="Required" class="form-control"><br>';
                $songForm .= '<label>Structure : </label><input type="text" name="structure" maxlength="75" class="form-control"><br>';
                $songForm .= '<label>Intro : </label><input type="text" name="intro" maxlength="75" class="form-control"><br>';
                $songForm .= '<label>Type : </label><input type="text" name="type" maxlength="75" placeholder="Required" class="form-control"><br>';
                $songForm .= '<label>Info : </label><input type="text" name="info" maxlength="75" class="form-control"><br>';
                $songForm .= '<label>Duration : </label><input type="text" name="duration" class="form-control" pattern="[0-9][0-9]:[0-9][0-9]" placeholder="mm:ss" title="mm:ss"></div><br>';
                $songForm .= '<label id="inst">PPT : </label><input type="checkbox" name="ppt"><br><br>';

                //finding all the instruments available to the band
                $sqlBandMembers = 'SELECT i.instrumentID, i.instrumentDesc, i.person FROM instrument i JOIN bandDesc b ON i.instrumentID=b.instrumentID WHERE b.bandID = ' . $bandNo;

                $result = $conn->query($sqlBandMembers);

                while ($row = $result->fetch_assoc()) {
                    //while there are more instruments, create a checkbox option for the current instrument and add the instrument to the availability list
                    $availableInstruments[] = $row['instrumentID'];
                    $songForm .= '<label id="inst"><input type="checkbox" name="instrument_' . $row['instrumentID'] . '">   ' . $row['instrumentDesc'] . ' ' . $row['person'] . '</label>';
                }
                $songForm .= '<br><br><input type="submit"></form>';          //finalising the form

                echo $songForm;         //displaying the form that has just been created

                //if the form has been completed and the data has been POSTed with a title, type and duration
                if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['title'] != null && $_POST['type'] != null && $_POST['duration'] != null) {
                    //create an SQL insertion statement for the item requiring it
                    $SQLItem = 'INSERT INTO item (title, structure, intro, type, info, duration, ppt) VALUES ("';
                    $SQLItem .= $_POST['title'] . '","';
                    $SQLItem .= $_POST['structure'] . '","';
                    $SQLItem .= $_POST['intro'] . '","';
                    $SQLItem .= $_POST['type'] . '","';
                    $SQLItem .= $_POST['info'] . '","';

                    //ensuring the duration box has a : present and is a time
                    if (strpos($_POST['duration'], ':') != false) {
                        $SQLItem .= '00:' . $_POST['duration'] . '",';
                    }

                    //checking whether there is a ppt related to the item
                    if (isset($_POST['ppt'])) {
                        $SQLItem .= '1';
                    } else {
                        $SQLItem .= '0';
                    }

                    $SQLItem .= ')';

                    $conn->query($SQLItem);         //running the query in order to store the item

                    $itemID;                        //used to store the items id

                    $result = $conn->query("SELECT itemID FROM item ORDER BY itemID DESC LIMIT 1");     //getting the item id of the last item, just created

                    while ($row = $result->fetch_assoc()) {
                        $itemID = $row['itemID'];
                    }

                    foreach ($availableInstruments as $i)       //for each of the available instruments
                    {
                        $check = 'instrument_' . $i;              //forming a check for the checkboxes to be compared to

                        if (isset($_POST[$check]))              //if the next checkbox in the sequence is checked
                        {
                            //add the instrument and item into songbad to store the fact that instrument plays that item
                            $SQLInstrument = 'INSERT INTO songBand (itemID, instrumentID) VALUES (' . $itemID . ',' . $i . ')';
                            $conn->query($SQLInstrument);
                        }
                    }
                }
            }
        ?>
    </div>

    <div id="rightColumn" class="col-md-4">
        <?php
            //sql to get all the titles of songs saved so far, visual confirmation
            $sqlForCurrentSongs = "SELECT itemID, title FROM item ORDER BY itemID DESC";

            $songResult = $conn->query($sqlForCurrentSongs);

            echo '<h2>Current Items</h2> <ul>';      //display the title

            while ($row = $songResult->fetch_assoc())
            {
                //making a list of the titles of songs, hyperlinks to edit the song
                echo '<li><a href="songEdit.php?item='.$row["itemID"].'">'.$row["title"].'</a></li>';
            }

            echo '</ul>';

        ?>
    </div>
</body>
