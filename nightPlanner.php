<title>Plan Night</title>
<body>
    <?php
        include 'heading.php';     //includes the heading
        include 'server_connection.php';	//includes the server connection file

        $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
        }

        $database = $dbname;        //database is dbName form server_connection

        if (isset($_GET['night'])) {

            $sqlCCCEvent = 'SELECT CCCIndex FROM event WHERE eventDate="'.$_GET['night'].'"';      //searching for the event  ID for CCC website

            $result = $conn->query($sqlCCCEvent);               //running said query

            $cccIndex;      //variable to store index

            while ($row = $result->fetch_assoc())
            {
                $cccIndex = $row['CCCIndex'];       //setting the index to the result
            }

            echo '<div class="container" id="options">';        //creating div to provide the required operations for planning the night

                echo '<h2>Planning : '.$_GET['night'].'<a href="nightPlanner.php" class="btn btn-default btn-inline" style="margin-left: 20px;">Change night</a></h2>';         //showing which night is being planned

                echo '<br>';

                echo '<h3>To plan night : </h3>';       //user prompts

                echo '<h4>Only use the following types ; Song, Act, Downtime, Talk - THANKS!</h4>';

                echo '<p>Add songs as normal, all end in "(TIH)". For Act or Downtime make item type as such (Act / Downtime) and make detail "x Min(s)". For Talk, simply make item type Talk.</p>';         //user advice

                echo '<a href="https://www.carrubbers.org/eventplans/#/id/'.$cccIndex.'" target="_blank" class="btn btn-default btn-block btn-info">Plan Night (performed on carrubbers.org)</a>';       //creating button to take to the event on the ccc website

                echo '<h3>Once finished, return to this page and load the schedule into the TIH database : </h3>';        //user prompts

                echo '<a href="loadCCCToTIH.php?night='.$cccIndex.'&tihNight='.$_GET['night'].'" class="btn btn-default btn-block btn-warning">Load plan to TIH database (**MUST PERFORM BEFORE RUNNING NIGHT!**)</a>';         //button to load plan into the db

                echo '<h3>Run the night : </h3>';       //user prompt

                echo '<a href="nightRunner.php?night='.$_GET['night'].'" class="btn btn-default btn-block btn-success">Run Night</a>';        //hyperlink to run the night
	
            echo '</div>';      //closing the div

        } else {

            echo '<div id="bandSearch">';       //making div for the dropdown to chose what event to edit

                echo '<h1>Night Planner</h1><br>';

                echo '<label>Select Event : </label>';      //label for form

                echo '<form action="" method="get">';      //form which returns to current page but by post, triggering the first part of if statement

                    $select = '<select name="night">';          //dropdown to select what night is being edited

                    $sql = 'SELECT * FROM event';                //selecting all events in the system

                    $result = $conn->query($sql);               //running the query

                    while ($row = $result->fetch_assoc())
                    {
                        $select .= '<option value="' . $row['eventDate'] . '">' . $row['eventDate'] . '</option>';    //add each band to the dropdown list
                    }

                    $select .= '</select>';                     //ending dropdown

                    echo $select;       //displays the dropdown list created

                    echo '<input type="submit">';       //adding a submit button

                echo '</form>';                     //ending the form

            echo '</div>';                      //ending the div

        }

    ?>

</body>
