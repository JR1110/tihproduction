<head>
    <link rel="stylesheet" href="custom.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <nav class="navbar navbar-default navbar-inverse">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <?php
                if (basename($_SERVER['PHP_SELF']) != "stage.php") {
                    echo '<li><a href="songEntry.php">Enter Items</a></li>';
                    echo '<li><a href="nightPlanner.php">Plan Night</a></li>';
                    echo '<li><a href="nightRunner.php">Run Night</a></li>';
                    echo '<li><a href="stage.php">Stage</a></li>';
                }
                ?>
            </ul>
            <?php echo '<h3 style="text-align: right; padding-right:20px; color: #ffffff;">Current Time : '.date("H:i").'</h3>' ?>
        </div>
    </nav>
</head>