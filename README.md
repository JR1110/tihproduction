There Is Hope Production
Created by Josh R

THIS SYSTEM IS TO BE USED FOR THERE IS HOPE (tih)

Throughout the night both thte artists, tech, managers, guests and producer need to be kept informed of exact going ons, to this end the systeme currently being created here is in a direct replacemtn to the excel spreadsheet of old.

The main features required are :
          
	ABILITY TO PLAN A NIGHT
	ABILITY TO USE THAT PLAN TO RUN A NIGHT
	DURING RUNNING, CHANGE TO NEXT OR PREVIOUS SONG AS AND WHEN REQUIRED
	WORK OUT ERRORS FOR HOW FAR BEHIND SCHEDULE AT CURRENT TIME
	DISPLAY SUITABLE 'STAGE' OUTPUT SHOWING INFORMATION FOR BOTH STAGE AND TECH
		  
So far we have the following pages with a brief description : 

    custom.css - custom formatting css page
	event.php - event class
	heading.php - a constant heading used in all pages
	nightPlanner.php - where the niht is planned, allows for items to be added to the night
	nightRunner.php - where the night is run from (uses a lot of files in nightActions)
	notes.txt - used for note passing from producer to stage
	observe.php - NOT IN USE CURRENTLY! was used in an attempt to use the observer design pattern
	removeFromNight.php - called from night planner to remove an item from the night
	server_connection.php - used to store the server information for connecting to the database
	songEdit.php - used to edit a song
	songEntry.php - used to enter an item (not just songs) to the database 
	stage.php - used to display the information to the screen on stage or by tech
	stageCurrent.txt - stores what the stage thinks is the current item and date
		  
	nightActions/addDowntime.php - adds in selected downtime amount and fixes any timings
	nightActions/currentNo.txt - stores the current number and date of whats going on
	nightActions/fixTume.php - fixes the time in a schedule
	nightActions/nextItem.php - moves the current item to be the next item
	nightActions/notes.php - writes the notes to the text file
	nightActions/notes.txt - text file for notes
	nightActions/previousItem.php - moves the current item to be the previous one
	nightActions/startNight.php - starts the night at item 1
	
	
And on the list writing, TODO :

	Fix 'errors' on producer
	Try and get observer pattern working to avoid loads of refreshes
	General neatening of code

For more info please contact me!

Josh R