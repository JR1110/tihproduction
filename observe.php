<?php
    abstract class AbstractObserver {
        abstract function update(AbstractSubject $subject);
    }

    abstract class AbstractSubject {
        abstract function attach(AbstractObserver $observer);
        abstract function detach(AbstractObserver $observer);
        abstract function notify();
    }

    class PatternObserver extends AbstractObserver {
        public function __construct() { }

        public function update (AbstractSubject $subject)
        {
            //TODO send output to the observer
            $myFile = fopen('/var/www/html/stageInstructions.php', 'w') or die ('Unable to open :(');

            /*$currentNo = 1;

            for ($i = 0; $i < sizeof($subject->nightPlan); $i ++)
            {
                if ($subject->nightPlan[$i]['status'] == "current" )
                {
                    $currentNo = $i;
                    break;
                }
            }

            $txt = '<div class="container" id="firstThree"> \n <h3>CurrentItem : '.$subject->nightPlan[$currentNo]['title'].'</h3> \n </div>';*/

            $txt = $subject->nightPlan;

            fwrite($myFile, $txt);
        }
    }

    class PatternSubject extends AbstractSubject {
        public function __construct() { }

        private $nightPlan;
        private $observers = array();

        function attach(AbstractObserver $observer)
        {
            $this->observers[] = $observer;
        }

        function detach(AbstractObserver $observer)
        {
            foreach($this->observers as $obsKey => $obsVal)
            {
                if ($obsVal == $observer)
                {
                    unset($this->observers[$obsKey]);
                }
            }
        }

        function notify()
        {
            foreach($this->observers as $obs)
            {
                $obs->update($this);
            }
        }

        function updatePlan($nightPlans)
        {
            $this->nightPlan = $nightPlans;
            $this->notify();
        }

        function getItems()
        {
            return $this->nightPlan;
        }
    }

    $subject = new PatternSubject();            //creating a subject
    $observer = new PatternObserver();          //creating an observer

    $subject->attach($observer);                //attaching the observer to the subject

?>