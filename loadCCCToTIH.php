<title>Load CCC to TIH</title>
<?php

    /*
    * Function : loadCCCToTIH
    * Use : To load a planned event from Carrubbers into the There Is Hope DB
    * Created by : Josh R (05/8/17)
    *
    */

    include 'server_connection.php';	//includes the server connection file
    include 'event.php';      //including the event class

    $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
    }

    $database = $dbname;

    $event = $_GET['night'];        //gets the night from the URL

    $eventURL = 'https://www.carrubbers.org/eventplans/index.rest.php/eventItemDatum?EventId='.$event;      //getting the full URL for the event on the website

    $eventJSON = scraper($eventURL);        //running the screen scraper for all the JSON data

    $initialCleanSQL = 'DELETE eD.* FROM eventDetails eD JOIN event e ON eD.eventDate=e.eventDate WHERE e.CCCIndex ="'.$event.'"';      //removing all from the database which correlates to night being planned

    $conn->query($initialCleanSQL);         //running the db clean up

    $night;         //storing the night date

    $sqlGetEventDate = 'SELECT eventDate FROM event WHERE CCCIndex = "'.$event.'"';         //getting the current eventDate

    $nightResult = $conn->query($sqlGetEventDate);      //run sql for eventDate

    while ($row = $nightResult->fetch_assoc())
    {
        $night = $row['eventDate'];         //setting night to be the eventDate
    }

    $eventArray = json_decode($eventJSON, true);        //decoding the JSON screen Scrape

    $eventKeys = array_keys($eventArray);       //getting the keys of each item, their item number

    foreach ($eventKeys as $k)
    {
        $kType = $eventArray[$k]["Type"];       //getting the current key's type


        $startTime;         //start time for item

        if ($eventArray[$k]["Order"] == 1)      //if it is the first item
        {
            $startSQL = 'SELECT startTime FROM event WHERE CCCIndex="'.$event.'"';      //getting the start for the night

            $startResult = $conn->query($startSQL);         //run query

            while ($row = $startResult->fetch_assoc())
            {
                $startTime = $row['startTime'];       //make the start time the start time of the whole event
            }
        } else {        //if it isn't the first item
            $max = 0;                        //used to store the items id

            $sqlNextItem = 'SELECT orderOnNight FROM eventDetails WHERE eventDate = "'.$night.'" ORDER BY orderOnNight DESC LIMIT 1';

            $result = $conn->query($sqlNextItem);     //getting the item id of the last item, just created

            while ($row = $result->fetch_assoc())
            {
                $max = $row['orderOnNight'];            //saving the max order as max
            }

            $max = $max + 1;

            $SQLPreviousTime = 'SELECT AddTime(e,d) AS newExpectedTime FROM (SELECT e.expectedStart AS e, i.duration AS d FROM eventDetails e JOIN item i ON i.itemID=e.itemID WHERE e.orderOnNight = '.($max-1).' AND e.eventDate="'.$night.'") x';

            $result = $conn->query($SQLPreviousTime);

            while ($row = $result->fetch_assoc())
            {
                $startTime = $row['newExpectedTime'];
            }
        }

        $eventItemNo;       //stores the item number for the event

        if ($kType == "Song")       //if the item type is song
        {
            $sqlFindSong = 'SELECT itemID FROM item WHERE CCCIndex = '.$eventArray[$k]["SongId"];

            $songResult = $conn->query($sqlFindSong);

            while ($row = $songResult->fetch_assoc())
            {
                $eventItemNo = $row['itemID'];       //the item number is the song
            }

        } elseif ($kType == "Downtime") {       //if it is downtime
            $sqlFindDowntime = 'SELECT itemID FROM item WHERE type = "Downtime" AND info = "'.$eventArray[$k]["Detail"].'"';         //find the item no based on the detail provided

            $downtimeResult = $conn->query($sqlFindDowntime);       //running sql

            while ($row = $downtimeResult->fetch_assoc())
            {
                $eventItemNo = $row['itemID'];      //setting the item number to be the one just found
            }

            if ($eventItemNo == null)       //if still null
            {
                $eventItemNo = 20;          //make it item 20 (5 mins downtime!)
            }
        } elseif ($kType == "Act") {
            $sqlFindAct = 'SELECT itemID FROM item WHERE type = "Act" AND info = "'.$eventArray[$k]["Detail"].'"';         //find the item no based on the detail provided

            $actResult = $conn->query($sqlFindAct);       //running sql

            while ($row = $actResult->fetch_assoc())
            {
                $eventItemNo = $row['itemID'];      //setting the item number to be the one just found
            }

            if ($eventItemNo == null)       //if still null
            {
                $eventItemNo = 45;          //make it item 45 (30 min act)
            }
        } elseif ($kType == "Talk") {
            $sqlFindTalk = 'SELECT itemID FROM item WHERE title = "Talk"';         //find the item no based on the detail provided

            $talkResult = $conn->query($sqlFindTalk);       //running sql

            while ($row = $talkResult->fetch_assoc())
            {
                $eventItemNo = $row['itemID'];      //setting the item number to be the one just found
            }

        }

        $eventItem = new event($night, $eventItemNo, $eventArray[$k]["Order"], $startTime);       //making event

        $conn->query($eventItem->sqlCreator());         //saving event to the db
    }

	if (isset($_GET['nightPlan']))
	{
		header('Location: nightRunner.php?night='.$_GET['nightPlan']);
	} else {
		header('Location: nightPlanner.php?night='.$_GET['tihNight']);       //return to the night planner
	}


    function scraper( $url )        //screen scraping function for CCC event data
    {

        $JSON = file_get_contents( $url );         //variable to store the JSON created by the CCC website

        return $JSON;       //returning the JSON

    }

?>