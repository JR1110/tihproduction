-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2017 at 02:24 PM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thereishope`
--

-- --------------------------------------------------------

--
-- Table structure for table `band`
--

CREATE TABLE IF NOT EXISTS `band` (
`bandID` int(11) NOT NULL,
  `bandDesc` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `band`
--

INSERT INTO `band` (`bandID`, `bandDesc`) VALUES
(1, 'There is Hope House Band Week 1 2017'),
(2, 'There is Hope House Band Week 2 2017'),
(3, 'Spoken / Non Musical Item');

-- --------------------------------------------------------

--
-- Table structure for table `bandDesc`
--

CREATE TABLE IF NOT EXISTS `bandDesc` (
  `bandID` int(11) NOT NULL DEFAULT '0',
  `instrumentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bandDesc`
--

INSERT INTO `bandDesc` (`bandID`, `instrumentID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `eventDate` date NOT NULL DEFAULT '0000-00-00',
  `startTime` time DEFAULT NULL,
  `houseBandID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`eventDate`, `startTime`, `houseBandID`) VALUES
('2017-07-02', '13:30:00', 1),
('2017-08-07', '20:00:00', 1),
('2017-08-08', '20:00:00', 1),
('2017-08-09', '20:00:00', 1),
('2017-08-10', '20:00:00', 1),
('2017-08-11', '20:00:00', 1),
('2017-08-12', '20:00:00', 1),
('2017-08-14', '20:00:00', 2),
('2017-08-15', '20:00:00', 2),
('2017-08-16', '20:00:00', 2),
('2017-08-17', '20:00:00', 2),
('2017-08-18', '20:00:00', 2),
('2017-08-19', '20:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `eventDetails`
--

CREATE TABLE IF NOT EXISTS `eventDetails` (
  `eventDate` date NOT NULL DEFAULT '0000-00-00',
  `itemID` int(11) NOT NULL DEFAULT '0',
  `orderOnNight` int(11) DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `expectedStart` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventDetails`
--

INSERT INTO `eventDetails` (`eventDate`, `itemID`, `orderOnNight`, `status`, `expectedStart`) VALUES
('2017-07-02', 9, 1, '', '13:30:00'),
('2017-08-08', 9, 1, '', '20:00:00'),
('2017-08-08', 10, 2, '', '20:00:00'),
('2017-08-08', 11, 3, '', '20:00:00'),
('2017-08-08', 4, 4, '', '20:00:00'),
('2017-08-08', 1, 5, '', '20:04:10'),
('2017-08-08', 2, 6, '', '20:07:50'),
('2017-08-08', 3, 7, '', '20:12:14'),
('2017-08-08', 1, 8, '', '20:17:01'),
('2017-08-08', 4, 9, '', '20:20:41'),
('2017-08-08', 3, 10, '', '20:24:51'),
('2017-07-02', 10, 3, '', '13:40:00'),
('2017-07-02', 11, 4, '', '13:40:00'),
('2017-07-02', 4, 5, '', '13:40:00'),
('2017-07-02', 6, 8, '', '13:46:10'),
('2017-07-02', 3, 9, '', '13:50:05'),
('2017-07-02', 5, 10, '', '13:54:52'),
('2017-07-02', 7, 11, '', '13:58:52'),
('2017-07-02', 2, 12, 'current', '14:03:32'),
('2017-07-02', 1, 13, '', '14:07:56'),
('2017-07-02', 15, 14, '', '14:11:36'),
('2017-07-02', 13, 15, '', '14:12:06'),
('2017-07-02', 8, 16, '', '14:13:36'),
('2017-07-02', 14, 17, '', '14:20:36'),
('2017-07-02', 18, 18, '', '14:21:06'),
('2017-07-02', 4, 19, '', '14:24:06'),
('2017-07-02', 1, 20, '', '14:28:16'),
('2017-07-02', 3, 21, '', '14:31:56'),
('2017-07-02', 23, 22, '', '14:36:43'),
('2017-07-02', 4, 23, '', '14:51:43'),
('2017-07-02', 22, 2, '', '13:30:00'),
('2017-07-02', 15, 24, '', '14:55:53'),
('2017-07-02', 13, 25, '', '14:56:23'),
('2017-07-02', 8, 26, '', '14:57:53'),
('2017-07-02', 14, 27, '', '15:04:53'),
('2017-07-02', 5, 28, '', '15:05:23'),
('2017-07-02', 4, 29, '', '15:09:23'),
('2017-07-02', 5, 30, '', '15:13:33'),
('2017-07-02', 16, 7, '', '13:45:10'),
('2017-07-02', 16, 6, '', '13:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `instrument`
--

CREATE TABLE IF NOT EXISTS `instrument` (
`instrumentID` int(11) NOT NULL,
  `instrumentDesc` varchar(20) DEFAULT NULL,
  `person` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instrument`
--

INSERT INTO `instrument` (`instrumentID`, `instrumentDesc`, `person`) VALUES
(1, 'Piano', NULL),
(2, 'Accoustic Guitar', NULL),
(3, 'Electric Guitar', NULL),
(4, 'Bass', NULL),
(5, 'Synth', NULL),
(6, 'Drums', NULL),
(7, 'Violin', NULL),
(8, 'Whistle', NULL),
(9, 'Singer', 'Rach M'),
(10, 'Singer', 'Ruth'),
(11, 'Singer', 'Laura'),
(12, 'Singer', 'Suzie'),
(13, 'Singer', 'Alec');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`itemID` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `structure` varchar(75) DEFAULT NULL,
  `intro` varchar(75) DEFAULT NULL,
  `type` varchar(75) DEFAULT NULL,
  `info` varchar(75) DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `ppt` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemID`, `title`, `structure`, `intro`, `type`, `info`, `duration`, `ppt`) VALUES
(1, 'Iris', 'VCVCIC', 'Acc Guitar', 'Band, Chilled', '', '00:03:40', 0),
(2, 'Who you are', '', '', 'Band, Chilled', '', '00:04:24', 0),
(3, 'I won''t let you go', 'VCVCBC', 'Acc Guitar', 'Band, Chilled', '', '00:04:47', 0),
(4, 'Dare you to move', 'VCVCBC', '', 'Band, Chilled', '', '00:04:10', 0),
(5, 'Skin', '', '', 'Band, Chilled', '', '00:04:00', 0),
(6, 'Give me something', '', '', 'Band, Chilled', '', '00:03:55', 0),
(7, 'Scare away the dark', '', '', 'Band, Chilled', '', '00:04:40', 0),
(8, 'Talk - Dave A', '', '', 'Talk', '', '00:07:00', 1),
(9, 'Evening Preperations', '', '', 'Pre-opening', '', '00:00:00', 0),
(10, 'Ready to go?', '', '', 'Pre-opening', '', '00:00:00', 0),
(11, 'Go!', '', '', 'Openning', '', '00:00:00', 0),
(12, 'Spotify', '', '', 'After Closing', '', '00:00:00', 0),
(13, 'Talk - Preamble', '', '', 'Talk', '', '00:01:30', 1),
(14, 'Speaker leave stage', '', '', 'Talk', '', '00:00:30', 0),
(15, 'Speaker to stage', '', '', 'Talk', '', '00:00:30', 0),
(16, 'Downtime (1 min)', '', '', 'Downtime', '', '00:01:00', 0),
(17, 'Downtime (2 mins)', '', '', 'Downtime', '', '00:02:00', 0),
(18, 'Downtime (3 mins)', '', '', 'Downtime', '', '00:03:00', 0),
(19, 'Downtime (4 mins)', '', '', 'Downtime', '', '00:04:00', 0),
(20, 'Downtime (5 mins)', '', '', 'Downtime', '', '00:05:00', 0),
(21, 'Downtime (7 mins)', '', '', 'Downtime', '', '00:07:00', 0),
(22, 'Downtime (10 mins)', '', '', 'Downtime', '', '00:10:00', 0),
(23, 'Downtime (15 mins)', '', '', 'Downtime', '', '00:15:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `songBand`
--

CREATE TABLE IF NOT EXISTS `songBand` (
  `itemID` int(11) NOT NULL DEFAULT '0',
  `instrumentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `songBand`
--

INSERT INTO `songBand` (`itemID`, `instrumentID`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 5),
(6, 5),
(7, 5),
(1, 6),
(2, 6),
(3, 6),
(4, 6),
(5, 6),
(6, 6),
(7, 6),
(1, 9),
(2, 9),
(3, 9),
(4, 9),
(5, 9),
(6, 9),
(7, 9),
(1, 10),
(2, 10),
(3, 10),
(4, 10),
(5, 10),
(6, 10),
(7, 10),
(2, 11),
(3, 11),
(4, 11),
(5, 11),
(6, 11),
(7, 11);

-- --------------------------------------------------------

--
-- Table structure for table `Songlists`
--

CREATE TABLE IF NOT EXISTS `Songlists` (
  `Songtitle` text NOT NULL,
  `Songlength` int(11) NOT NULL,
  `Singer1` text NOT NULL,
  `Singer2` text NOT NULL,
  `Singer3` int(11) NOT NULL,
  `Singer4` int(11) NOT NULL,
  `Piano` int(11) NOT NULL,
  `Keyboard` int(11) NOT NULL,
  `Drums` int(11) NOT NULL,
  `Elecguitar` int(11) NOT NULL,
  `Accguitar` int(11) NOT NULL,
  `Violin` int(11) NOT NULL,
  `Flute` int(11) NOT NULL,
  `Other1` int(11) NOT NULL,
  `Other2` int(11) NOT NULL,
  `SongID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Songlists`
--

INSERT INTO `Songlists` (`Songtitle`, `Songlength`, `Singer1`, `Singer2`, `Singer3`, `Singer4`, `Piano`, `Keyboard`, `Drums`, `Elecguitar`, `Accguitar`, `Violin`, `Flute`, `Other1`, `Other2`, `SongID`) VALUES
('0', 0, '0', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('God is great1', 3, 'Sarah', 'Rachel', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('Godisgreat1', 3, 'sarah', 'rachel', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('God is great 2', 3, 'Rachedl', 'Sarah', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `band`
--
ALTER TABLE `band`
 ADD PRIMARY KEY (`bandID`);

--
-- Indexes for table `bandDesc`
--
ALTER TABLE `bandDesc`
 ADD PRIMARY KEY (`bandID`,`instrumentID`), ADD KEY `instrumentID` (`instrumentID`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`eventDate`), ADD KEY `houseBandID` (`houseBandID`);

--
-- Indexes for table `eventDetails`
--
ALTER TABLE `eventDetails`
 ADD KEY `eventDetails_ibfk_1` (`eventDate`), ADD KEY `eventDetails_ibfk_2` (`itemID`);

--
-- Indexes for table `instrument`
--
ALTER TABLE `instrument`
 ADD PRIMARY KEY (`instrumentID`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`itemID`);

--
-- Indexes for table `songBand`
--
ALTER TABLE `songBand`
 ADD PRIMARY KEY (`itemID`,`instrumentID`), ADD KEY `instrumentID` (`instrumentID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `band`
--
ALTER TABLE `band`
MODIFY `bandID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `instrument`
--
ALTER TABLE `instrument`
MODIFY `instrumentID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `itemID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bandDesc`
--
ALTER TABLE `bandDesc`
ADD CONSTRAINT `bandDesc_ibfk_1` FOREIGN KEY (`bandID`) REFERENCES `band` (`bandID`),
ADD CONSTRAINT `bandDesc_ibfk_2` FOREIGN KEY (`instrumentID`) REFERENCES `instrument` (`instrumentID`);

--
-- Constraints for table `event`
--
ALTER TABLE `event`
ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`houseBandID`) REFERENCES `band` (`bandID`);

--
-- Constraints for table `eventDetails`
--
ALTER TABLE `eventDetails`
ADD CONSTRAINT `eventDetails_ibfk_1` FOREIGN KEY (`eventDate`) REFERENCES `event` (`eventDate`),
ADD CONSTRAINT `eventDetails_ibfk_2` FOREIGN KEY (`itemID`) REFERENCES `item` (`itemID`);

--
-- Constraints for table `songBand`
--
ALTER TABLE `songBand`
ADD CONSTRAINT `songBand_ibfk_1` FOREIGN KEY (`itemID`) REFERENCES `item` (`itemID`),
ADD CONSTRAINT `songBand_ibfk_2` FOREIGN KEY (`instrumentID`) REFERENCES `instrument` (`instrumentID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
