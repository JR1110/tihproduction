<?php
    include 'server_connection.php';	//includes the server connection file

    $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
    }

    $database = $dbname;

    if (isset($_GET['oON']) && isset($_GET['night']))
    {
        $itemToBeRemoved = $_GET['oON'];        //getting the number to be removed
        $night = $_GET['night'];                //getting the night on which it is to be removed from

        $sqlRemove = 'DELETE FROM eventDetails WHERE eventDate = "'.$night.'" AND orderOnNight = '.$itemToBeRemoved;      //deleting the event to be removed
        $conn->query($sqlRemove);

        $itemsToShift = 'SELECT * FROM eventDetails WHERE eventDate = "'.$night.'" AND orderOnNight > '. $itemToBeRemoved;        //finding all the items on after the deleted one

        $result = $conn->query($itemsToShift);

        while ($row = $result->fetch_assoc())
        {
            $newOrder = $row['orderOnNight'] - 1;

            $sqlShift = 'UPDATE eventDetails SET orderOnNight = '.$newOrder.' WHERE eventDate = "'.$night.'" AND orderOnNight = '.$row['orderOnNight'].' AND itemID = '.$row['itemID'];
            $conn->query($sqlShift);
        }

        fixTime($conn, $_GET['night']);

        header('Location: nightPlanner.php?night='.$night);
    }

    function fixTime($conn, $event)
    {
        $prevItem = 1;

        $SQLCount = 'SELECT COUNT(*) AS c FROM eventDetails WHERE eventDate ="'.$event.'"';
        $l = 1;

        $result = $conn->query($SQLCount);

        while ($r = $result->fetch_assoc())
        {
            $l = $r['c'];
        }


        for ($i = 1; $i <= $l; $i++)
        {
            $SQLGetRightTime = 'SELECT AddTime(e,d) AS newExpectedTime FROM (SELECT e.expectedStart AS e, i.duration AS d FROM eventDetails e JOIN item i ON i.itemID=e.itemID WHERE e.orderOnNight = '.($prevItem).' AND e.eventDate="'.$_GET['night'].'") x';

            $newTime = '';

            $result = $conn->query($SQLGetRightTime);

            while ($row = $result->fetch_assoc()) {
                $newTime = $row['newExpectedTime'];
            }

            echo $newTime.'<-- New Time, SQL -->';

            $SQLFixTime = 'UPDATE eventDetails SET expectedStart = "' . $newTime . '" WHERE eventDate = "' . $event . '" AND orderOnNight = ' . ($prevItem + 1);

            echo $SQLFixTime.'<br>';

            $conn->query($SQLFixTime);

            $prevItem++;
        }
    }
?>