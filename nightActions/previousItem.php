<?php
/*
 * Function : previousItem
 * Use : to move the previous item on to become the current item (current = current - 1)
 * Created by : Josh R (27/6/17)
 *
 */

    include '../server_connection.php';	//includes the server connection file

    $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
    }

    $database = $dbname;

    $currentItemNo;
    $SQLFinder = 'SELECT orderOnNight FROM eventDetails WHERE status="current" AND eventDate ="'.$_GET['night'].'"';

    $result = $conn->query($SQLFinder);

    while($row = $result->fetch_assoc())
    {
        $currentItemNo = $row['orderOnNight'];
    }

    $newCurrentItem = $currentItemNo - 1;

    $SQLRemoveCurrent = 'UPDATE eventDetails SET status = "" WHERE orderOnNight='.$currentItemNo.' AND eventDate = "'.$_GET['night'].'"';
    $SQLNewItem = 'UPDATE eventDetails SET status = "current" WHERE orderOnNight='.$newCurrentItem.' AND eventDate = "'.$_GET['night'].'"';

    $conn->query($SQLRemoveCurrent);
    $conn->query($SQLNewItem);

    $myFile = fopen('currentNo.txt', 'w') or die ('Unable to open');

    $txt = $newCurrentItem.'|'.$_GET['night'];

    fwrite($myFile, $txt);

    fclose($myFile);

    header('Location: ../nightRunner.php?night='.$_GET['night']);
?>