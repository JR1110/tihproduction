<?php
/*
 * Function : startNight
 * Use : to start the night by setting item one to be current
 * Created by : Josh R (2/6/17)
 *
 */

    include '../server_connection.php';	//includes the server connection file

    $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
    }

    $database = $dbname;

    $firstSQL = 'UPDATE eventDetails SET status = "" WHERE eventDate="'.$_GET['night'].'"';

    $conn->query($firstSQL);

    $SQL = 'UPDATE eventDetails SET status="current" WHERE eventDate="'.$_GET['night'].'" AND orderOnNight = 1';

    $conn->query($SQL);

    $myFile = fopen('currentNo.txt', 'w') or die ('Unable to open');

    $txt = '1|'.$_GET['night'];

    fwrite($myFile, $txt);

    fclose($myFile);

    header('Location: ../nightRunner.php?night='.$_GET['night']);
?>