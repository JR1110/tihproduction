<?php
    include '../time.php';

    function fixTime($conn, $event)
    {
        $prevItem = 1;

        $SQLCount = 'SELECT COUNT(*) AS c FROM eventDetails WHERE eventDate ="'.$event.'"';
        $l = 1;

        $result = $conn->query($SQLCount);

        while ($r = $result->fetch_assoc())
        {
            $l = $r['c'];
        }


        for ($i = 1; $i < $l; $i++)
        {
            $newTime = new time(0, 0, 0);

            $SQLGetRightTime = 'SELECT HOUR(i.duration) as ih, HOUR(e.expectedStart) as eh, MINUTE(i.duration) as im, MINUTE(e.expectedStart) as em, SECOND(i.duration) as ise, SECOND(e.expectedStart) as es FROM item i JOIN eventDetails e ON i.itemID = e.itemID WHERE e.orderOnNight = ' . $prevItem;

            $result = $conn->query($SQLGetRightTime);

            while ($row = $result->fetch_assoc()) {
                $newTime->hours = $row['eh']+$row['ih'];
                $newTime->minutes = $row['em']+$row['im'];
                $newTime->seconds = $row['es']+$row['ise'];
            }

            if ($newTime->seconds > 60)                                                    //if the seconds are over 60
            {
                $newTime->minutes = $newTime->minutes + 1;                                    //add on a minute
                $newTime->seconds = $newTime->seconds - 60;                                   //take away 60 seconds
            } elseif ($newTime->minutes > 60) {                                            //if the minutes are over 60
                $newTime->hours = $newTime->hours + 1;                                        //add an hour on
                $newTime->minutes = $newTime->minutes - 60;                                   //take away 60 minutes
            }

            $formattedTime = '' . $newTime->hours . ':' . $newTime->minutes . ':' . $newTime->seconds;

            $SQLFixTime = 'UPDATE eventDetails SET expectedStart = "' . $formattedTime . '" WHERE eventDate = "' . $event . '" AND orderOnNight = ' . ($prevItem + 1);

            $conn->query($SQLFixTime);

            $prevItem++;
        }
    }

?>