<?php
    /*
    * Function : addDowntime
    * Use : to add downtime into the schedule on the fly
    * Created by : Josh R (27/6/17)
    *
    */

    include '../server_connection.php';	//includes the server connection file

    $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
    }

    $database = $dbname;

    $currentItemNo;
    $SQLFinder = 'SELECT orderOnNight FROM eventDetails WHERE status="current" AND eventDate ="'.$_GET['night'].'"';

    $result = $conn->query($SQLFinder);

    while($row = $result->fetch_assoc())
    {
        $currentItemNo = $row['orderOnNight'];
    }

    $SQLUpdateRemaining = 'SELECT * FROM eventDetails WHERE orderOnNight > '.$currentItemNo;
    $result = $conn->query($SQLUpdateRemaining);

    while ($row = $result->fetch_assoc())
    {
        $newOrder = $row['orderOnNight'] + 1;
        $sqlShift = 'UPDATE eventDetails SET orderOnNight = '.$newOrder.' WHERE eventDate = "'.$_GET['night'].'" AND orderOnNight = '.$row['orderOnNight'].' AND itemID = '.$row['itemID'];
        $conn->query($sqlShift);
    }

    $downtimeItemNo = $currentItemNo + 1;

    $SQLInsertDowntime = 'INSERT INTO eventDetails (eventDate, itemID, orderOnNight) VALUES ("'.$_GET['night'].'", '.$_GET['downtime'].', '.$downtimeItemNo.')';
    $conn->query($SQLInsertDowntime);

    fixTime($conn, $_GET['night']);

    header('Location: ../nightRunner.php?night='.$_GET['night']);


    function fixTime($conn, $event)
    {
        $prevItem = 1;

        $SQLCount = 'SELECT COUNT(*) AS c FROM eventDetails WHERE eventDate ="'.$event.'"';
        $l = 1;

        $result = $conn->query($SQLCount);

        while ($r = $result->fetch_assoc())
        {
            $l = $r['c'];
        }


        for ($i = 1; $i <= $l; $i++)
        {
            $SQLGetRightTime = 'SELECT AddTime(e,d) AS newExpectedTime FROM (SELECT e.expectedStart AS e, i.duration AS d FROM eventDetails e JOIN item i ON i.itemID=e.itemID WHERE e.orderOnNight = '.($prevItem).' AND e.eventDate="'.$_GET['night'].'") x';

            $newTime = '';

            $result = $conn->query($SQLGetRightTime);

            while ($row = $result->fetch_assoc()) {
                $newTime = $row['newExpectedTime'];
            }

            echo $newTime.'<-- New Time, SQL -->';

            $SQLFixTime = 'UPDATE eventDetails SET expectedStart = "' . $newTime . '" WHERE eventDate = "' . $event . '" AND orderOnNight = ' . ($prevItem + 1);

            echo $SQLFixTime.'<br>';

            $conn->query($SQLFixTime);

            $prevItem++;
        }
    }
?>