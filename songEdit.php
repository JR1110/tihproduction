<title>Edit Songs</title>
<body>
    <?php
        include 'heading.php';     //includes the heading
        include 'server_connection.php';	//includes the server connection file

        $conn = new mysqli($servername, $username, $password, $dbname);		//uses variables from the server_connection.php file

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);	//stops if no connection could be created
        }

        $database = $dbname;
    ?>

    <div class="container">
        <?php
        if (isset($_GET['item']))                           //if a band has been chosen
        {
            echo '<h2>Edit Item</h2>';

            $getDetailsSQL = 'SELECT * FROM item WHERE itemID ='.$_GET['item'];         //getting all the items details

            $availableInstruments = array();                //array that will store all the available instruments for that band

            $songDetailsResult = $conn->query($getDetailsSQL);          //running the SQL query

            $songForm;      //creating the form to be added to

            while ($row = $songDetailsResult -> fetch_assoc()) {
                //Creating the form, using inline if statements to fill in details database currently has
                $songForm .= '<form action ="" method="post">';
                $songForm .= '<div class="form-group"><label>Title : </label><input type="text" name="title" maxlength="100" value="'.($row["title"] != null ? $row["title"] : "");
                $songForm .= '" class="form-control"><br>';
                $songForm .= '<label>Structure : </label><input type="text" name="structure" maxlength="75" value="'.($row["structure"] != null ? $row["structure"] : "");
                $songForm .= '" class="form-control"><br>';
                $songForm .= '<label>Intro : </label><input type="text" name="intro" maxlength="75" value="'.($row["intro"] != null ? $row["intro"] : "");
                $songForm .= '" class="form-control"><br>';
                $songForm .= '<label>Type : </label><input type="text" name="type" maxlength="75" placeholder="Required" value="'.($row["type"] != null ? $row["type"] : "");
                $songForm .= '" class="form-control"><br>';
                $songForm .= '<label>Info : </label><input type="text" name="info" maxlength="75" value="'.($row["info"] != null ? $row["info"] : "");
                $songForm .= '" class="form-control"><br>';
                $songForm .= '<label>Duration : </label><input type="text" name="duration"  pattern="[0-9][0-9]:[0-9][0-9]:[0-9][0-9]" placeholder="hh:mm:ss" title="hh:mm:ss" value="'.($row["duration"] != null ? $row["duration"] : "");
                $songForm .= '" class="form-control"></div><br>';

                if ($row['ppt'] == 0) {
                    $songForm .= '<label id="inst">PPT : </label><input type="checkbox" name="ppt"><br><br>';
                } elseif ($row['ppt'] == 1) {
                    $songForm .= '<label id="inst">PPT : </label><input type="checkbox" name="ppt" checked><br><br>';
                }
            }
            //finding all the instruments available to the band
            $sqlBandMembers = 'SELECT i.instrumentID, i.instrumentDesc, i.person FROM instrument i';

            $result = $conn->query($sqlBandMembers);

            while ($row = $result->fetch_assoc()) {
                $availableInstruments[] = $row['instrumentID'];         //adding to the available instruments

                $sqlIsPlaying = 'SELECT * FROM songBand WHERE itemID = '.$_GET['item'].' AND instrumentID = '.$row['instrumentID'];      //finding out if the current instrument plays in the current item

                $playingRows = $conn->query($sqlIsPlaying);

                if (mysqli_num_rows($playingRows) == null) {
                    $songForm .= '<label id="inst"><input type="checkbox" name="instrument_' . $row['instrumentID'] . '">   ' . $row['instrumentDesc'] . ' ' . $row['person'] . '</label>';
                } else {
                    $songForm .= '<label id="inst"><input type="checkbox" checked name="instrument_' . $row['instrumentID'] . '">   ' . $row['instrumentDesc'] . ' ' . $row['person'] . '</label>';
                }
            }
            $songForm .= '<br><br><input type="submit"></form>';          //finalising the form

            echo $songForm;         //displaying the form that has just been created

            //if the form has been completed and the data has been POSTed with a title, type and duration
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['title'] != null && $_POST['type'] != null && $_POST['duration'] != null) {
                //create an SQL update statement for the item requiring it
                $SQLItem = 'UPDATE item SET ';
                $SQLItem .= 'title = "'.$_POST['title'] . '",';
                $SQLItem .= 'structure = "'.$_POST['structure'] . '",';
                $SQLItem .= 'intro = "'.$_POST['intro'] . '",';
                $SQLItem .= 'type = "'.$_POST['type'] . '",';
                $SQLItem .= 'info = "'.$_POST['info'] . '",';

                //ensuring the duration box has a : present and is a time
                if (strpos($_POST['duration'], ':') != false) {
                    $SQLItem .= 'duration = "' . $_POST['duration'] . '",';
                }

                //checking whether there is a ppt related to the item
                if (isset($_POST['ppt'])) {
                    $SQLItem .= 'ppt = 1';
                } else {
                    $SQLItem .= 'ppt = 0';
                }

                $SQLItem .= ' WHERE itemID = '.$_GET['item'];       //updating said item ID

                $conn->query($SQLItem);         //running the query in order to store the item


                foreach ($availableInstruments as $i)       //for each of the available instruments
                {
                    $check = 'instrument_' . $i;              //forming a check for the checkboxes to be compared to

                    if (isset($_POST[$check]))              //if the next checkbox in the sequence is checked
                    {
                        //add the instrument and item into songbad to store the fact that instrument plays that item
                        $SQLInstrument = 'INSERT INTO songBand (itemID, instrumentID) VALUES (' . $_GET['item'] . ',' . $i . ')';
                        $conn->query($SQLInstrument);
                    } else {
                        $SQLInstrument = 'DELETE FROM songBand WHERE itemID = '.$_GET['item'].' AND instrumentID = '.$i;
                        $conn->query($SQLInstrument);
                    }
                }

                if (isset($_GET['night']))
                {
                    header('Location: nightRunner.php?night='.$_GET['night']);
                } else {
                    header('Location: songEntry.php');
                }
            }
        }
        ?>
    </div>
</body>
