<?php
    
    include'server_connection.php'; 		//server connection

    $conn = new mysqli($servername, $username, $password, $dbname);         //db connection

    if ($conn->connect_error) {
        die("connection failed: ".$conn->connect_error);
    }

    $getCCCSql = 'SELECT CCCIndex from event WHERE eventDate="'.$_GET['night'].'"';     //SQL for getting the carrubbers event number

    $result = $conn->query($getCCCSql);

    $cI;        //stores the CCC index

    while ($row = $result->fetch_assoc())
    {
        $cI = $row['CCCIndex'];         //set the CCC index
    }
	
    if ($_GET['use'] == "Edit")         //if it was the edit button
    {
		header('Location: https://www.carrubbers.org/eventplans/#/id/'.$cI);        //go to the website
    } elseif ($_GET['use'] == "Load") {         //if it was the reload button
        header('Location: loadCCCToTIH.php?night='.$cI.'&nightPlan='.$_GET['night']);        //relolads the plan
    }
	
	//header ('Location: nightRunner.php?night='.$_GET['night']);
	
?>